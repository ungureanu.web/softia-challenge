Description
-----------

Stock management was intended to be in the database but opted for a 
simulated sensor reading aproach instead.

Instructions
------------

Installation
- run migrations
- run seeder 
    `php artisan db:seed --class=dataTablesSeeder`

- list all available products and choose one
    `php artisan vending:list`
 
This is what we have for oyu today!
+----+-------------------+-------+-------+-------------+
| ID | Name              | Price | Stock | Ingredients |
+----+-------------------+-------+-------+-------------+
| 1  | Espresso          | 3     | 2     |             |
| 2  | Espresso lung     | 4     | 1     |             |
| 3  | Espresso cu lapte | 1     | 2     |             |
| 4  | Latte             | 4     | 1     |             |
| 5  | Mokaccino         | 1     | 1     |             |
| 6  | Caffè mocha       | 1     | 1     |             |
| 7  | Nes               | 1     | 1     |             |
+----+-------------------+-------+-------+-------------+

 Choose what you want to drink (only in stock)::
  [1] Espresso
  [2] Espresso lung
  [3] Espresso cu lapte
  [4] Latte
  [5] Mokaccino
  [6] Caffè mocha
  [7] Nes
 > 
 `
 
 