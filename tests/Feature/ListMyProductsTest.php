<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ListMyProductsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->artisan('vending:list')
            ->expectsQuestion('Choose what you want to drink (only in stock):', rand(1, 3))
            ->assertExitCode(0);
    }
}
