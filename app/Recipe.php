<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Recipe extends Pivot
{
    //
    protected $table = 'recipes';

}
