<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Softia\Challenge\CoffeeMachine\VendingMachine\Helpers\VendingHelper;

/**
 * Class ListMyProducts
 * @package App\Console\Commands
 */
class ListMyProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vending:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lists available products (id, name, price, content, availability/quantity) ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $machine = new VendingHelper();
        $products = $machine->getInventory();
        $inStock = [];
        foreach ($products as $product) {
            if ($product['stock'] > 0) {
                $inStock[$product['id']] = $product['name'];
            }
        }
        $this->info('This is what we have for oyu today!');
        $this->table(['ID', 'Name', 'Price', 'Stock', 'Ingredients'], $products);
        $option = $this->choice('Choose what you want to drink (only in stock):', $inStock);

        $selectedProduct = $machine->selectProduct((int)$option);

    }

}
