<?php

namespace Softia\Challenge\CoffeeMachine\VendingMachine\Helpers;


use App\Ingredient;
use App\Product;
use App\Recipe;
use Softia\Challenge\CoffeeMachine\VendingMachine\OrderInterface;
use Softia\Challenge\CoffeeMachine\VendingMachine\VendingMachineInterface;
use Softia\Challenge\CoffeeMachine\VendingMachine\Payments\BillInterface;

class VendingHelper implements VendingMachineInterface
{

    public function getInventory(): array
    {

        // get list of available products: name, price, content, availability/quantity


        // clean product array so it can be rendered to console
        $cleanProducts = [];

        $products = Product::all();

        // read available stock from integrated sensors
        $ingredients = $this->readAvailabilitySensors();

        foreach ($products as $product) {
            $product->ingredients;


            // we cycle every ingredient and determine if enough quantity available,
            // by considering the smallest value of all
            $min = null;
            foreach ($product->ingredients as $ingredient) {

                // calculus for available quantity of product based on sensor reading
                // we always choose the lowest stock for ingredient
                $ingredientStock = floor($ingredients[$ingredient->id]['available_quantity'] / $ingredient->pivot->quantity);

                if ($min === null) {
                    $min = $ingredientStock;
                } else {
                    if ($ingredientStock < $min) {
                        $min = $ingredientStock;
                    }
                }

            }

            if ($min === null) {
                $min = 0;
            }

            // setting product stock
            $product->stock = $min;

            $cleanProducts[] = [
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'stock' => $product->stock,
                'ingredients' => '',
            ];

        }

        return $cleanProducts;
    }

    /**
     * Because it is actually safer this way, the vending machine is equipped with weight sensor for all solid ingredients
     * and volume level sensors for liquids. This should be better than adding quantities in stock. PM me if in doubt :)
     * @param Product $product
     * @return Product
     */
    private function readAvailabilitySensors(): array
    {

        $ingredients = Ingredient::all();
        $cleanIngredients = [];
        foreach ($ingredients as $ingredient) {

            // dummy call here to dedicated sensor;
            // available quantity will be random between 0 and 30 for grams
            // and 0 and 200 for milliliters
            $ingredient->available_quantity = 0;
            switch ($ingredient->measuring_unit) {
                case 'g':
                    $ingredient->available_quantity = rand(0, 30);
                    break;

                case 'ml':
                    $ingredient->available_quantity = rand(0, 500);
                    break;
            }

            // add id as array key for easy reference and to avoid further iterations
            $cleanIngredients[$ingredient->id] = $ingredient->toArray();
        }

        return $cleanIngredients;

    }

    public function selectProduct(int $productId): bool
    {
        return true;
    }

    public function setSugarLevel(int $sugarLevel): bool
    {
        return false;
    }

    public function setMilkLevel(int $milkLevel): bool
    {
        return false;
    }

    public function confirmSelection(): bool
    {
        return false;
    }

    public function scanCard(): bool
    {
        return false;
    }

    public function takeBill(BillInterface $bill): bool
    {
        return false;
    }

    public function getCurrentOrder(): OrderInterface
    {
        return false;
    }
}
