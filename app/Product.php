<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    //
    protected $table = 'products';
    /**
     * @var int
     */
    private $stock;
    private $ingredients;

    public function ingredients()
    {
        return $this
            ->belongsToMany('App\Ingredient', 'recipes')
            ->using('App\Recipe')
            ->withPivot([
                'quantity'
            ]);
    }
}
