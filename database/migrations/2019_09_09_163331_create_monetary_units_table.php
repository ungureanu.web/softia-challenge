<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonetaryUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monetary_units', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('available_quantity');
            $table->unsignedTinyInteger('use_for_payment');
            $table->unsignedTinyInteger('use_for_change');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monetary_units');
    }
}
