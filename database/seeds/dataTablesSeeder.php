<?php

use App\Ingredient;
use App\Product;
use App\Recipe;
use Illuminate\Database\Seeder;

class dataTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // predefined product names and ingredients, but quanitites are randomly generated
        $productList = [
            [
                'name' => 'Espresso',
                'ingredients' => ['cafea|g', 'zahar|g', 'apa|ml']
            ],
            [
                'name' => 'Espresso lung',
                'ingredients' => ['cafea|g', 'zahar|g', 'apa|ml']
            ],
            [
                'name' => 'Espresso cu lapte',
                'ingredients' => ['cafea|g', 'zahar|g', 'apa|ml', 'lapte|ml']
            ],
            [
                'name' => 'Latte',
                'ingredients' => ['cafea|g', 'zahar|g', 'apa|ml', 'lapte|ml']
            ],
            [
                'name' => 'Mokaccino',
                'ingredients' => ['cafea|g', 'zahar|g', 'apa|ml', 'lapte|ml', 'scortisoara|g']
            ],
            [
                'name' => 'Caffè mocha',
                'ingredients' => ['cafea|g', 'zahar|g', 'apa|ml', 'lapte|ml']
            ],
            [
                'name' => 'Nes',
                'ingredients' => ['nes|g', 'zahar|g', 'apa|ml']
            ]
        ];


        $currProductId = 1;
        foreach ($productList as $product) {


//            DB::table('products')->insert([
//                'id' => $currProductId,
//                'name' => $product['name'],
//                'price' => rand(1, 5)
//            ]);

            Product::create([
                'id' => $currProductId,
                'name' => $product['name'],
                'price' => rand(1, 5)
            ]);

            // add ingredients in recipe
            foreach ($product['ingredients'] as $ingredient) {
                $ingredientData = explode('|', $ingredient);

                // first add ingredients (name and measuring unit)

                $ingredient = Ingredient::updateOrCreate([
                    'name' => $ingredientData[0],
                    'measuring_unit' => $ingredientData[1],
                ]);


                // adding the recipe items with random quantities
                Recipe::create([
                    'product_id' => $currProductId,
                    'ingredient_id' => $ingredient->id,
                    'quantity' => ($ingredient->measuring_unit == 'g') ? rand(1, 5) : rand(50, 200),
                ]);

            }
            $currProductId++;
        }
    }
}
